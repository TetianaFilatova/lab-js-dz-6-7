import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import { Family } from '../../models';
import { FamilyService } from '../../services/family.service';

const agePattern = /^(?!0)\d{1,2}$/;

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {}
  
  public ngOnInit(): void {
    this.initForm();
  }

  public addChild(): void {
    const child = this.createMember();
    (this.familyForm.get('children') as FormArray).push(child);
  }

  public removeChild(index: number): void {
    (this.familyForm.get('children') as FormArray).removeAt(index);
  }

  public submit(): void {
    const family = this.familyForm.value;
    this.addFamily(family);
    this.familyForm.reset();
  }

  private createMember(): FormGroup {
    return new FormGroup({
      name: new FormControl(null, [Validators.required]),
      age: new FormControl(null, [
        Validators.required,
        Validators.pattern(agePattern),
      ]),
    });
  }

  private initForm(): void {
    this.familyForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      father: this.createMember(),
      mother: this.createMember(),
      children: new FormArray([this.createMember()])
    });
  }

  private addFamily(family: Family): void {
    this.familyService.addFamily$(family).subscribe();
  }
 }
